import numpy as np

from pysph.tools.geometry import get_2d_tank, get_2d_block
from pysph.base.utils import get_particle_array

"""
This is an example for me to generate the particle positions properly. 
"""

# Container specs
container_height = 4.0
container_width = 4.0
nboundary_layers = 2

fluid_column_height = 2.0
fluid_column_width = 1.0

nu = 0.0
dx = 0.03
g = 9.81
vref = np.sqrt(2 * 9.81 * fluid_column_height)
co = 10.0 * vref
gamma = 7.0
alpha = 0.1
ro = 1000.0

beta = 0.0
B = co * co * ro / gamma
p0 = 1000.0

hdx = 1.3
h = hdx * dx
m = dx**2 * ro

print(h, m, ro)



xt, yt = get_2d_tank(dx=dx, length=container_width,
                     height=container_height, base_center=[2, 0],
                     num_layers=nboundary_layers)
xf, yf = get_2d_block(dx=dx, length=fluid_column_width,
                      height=fluid_column_height, center=[0.5, 1])

xf += dx
yf += dx

fluid = get_particle_array(
    name='fluid', 
    x=xf, 
    y=yf, 
    h=h, 
    m=m, 
    rho=ro
  )

boundary = get_particle_array(
  name='boundary', 
  x=xt, 
  y=yt, 
  h=h, 
  m=m,
  rho=ro
)

print(dir(fluid))
print(fluid.name)
# print(fluid.get_number_of_particles())
print(fluid.get_property_arrays().keys())


