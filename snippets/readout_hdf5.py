import h5py, os, sys

file = h5py.File('/home/david/projects/sph_project_root/codes/snippets/dam_break_2d_output/dam_break_2d_0.hdf5', 'r')


print(file.keys())



print("Keys of boundary particles")
print(file['particles']['boundary']['arrays'].keys())

print("Keys of fluid particles")
print(file['particles']['fluid']['arrays'].keys())

print("Attrs of solver data")
print(file['solver_data'].attrs.keys())


print(dir(file['solver_data']))
print(dir(file['solver_data'].attrs))
print('\n')
