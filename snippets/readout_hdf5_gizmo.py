import h5py, os, sys

datafile = h5py.File('/home/david/projects/sph_project_root/codes/gizmo-public-fork-david/ICs/example.hdf5', 'r')



print("Properties and attricutes of gizmo IC file")

print("Keys of the main file")
print(datafile.keys())

print("\nattrs of the headers dataset")
print(datafile['Header'].attrs.keys())

# Example on how to put that into a dict.
dicto = {}
dicto.update(datafile['Header'].attrs)

print("Keys of the PartType0 dataset")
print(datafile['PartType0'].keys())


print("Keys of the PartType3 dataset")
print(datafile['PartType3'].keys())


# Of the common properties:
print(datafile['PartType3']['Velocities'])
print("\t",datafile['PartType3']['Velocities'][()][0])

# Of the common properties:
print(datafile['PartType3']['Masses'])
print("\t",datafile['PartType3']['Masses'][()][0])

# Of the common properties:
print(datafile['PartType3']['Coordinates'])
print("\t",datafile['PartType3']['Coordinates'][()][0])


# Of the common properties:
print(datafile['PartType3']['ParticleIDs'])
print("\t",datafile['PartType3']['ParticleIDs'][()][0])
