(TeX-add-style-hook
 "sph_proj_beamer"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "graphics" "notes")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("natbib" "authoryear")))
   (add-to-list 'LaTeX-verbatim-environments-local "semiverbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "listings"
    "lstautogobble"
    "color"
    "pgfbaseimage"
    "pgfpages"
    "hyperref"
    "natbib")
   (TeX-add-symbols
    "binaryc"
    "vader"
    "mesa")
   (LaTeX-add-bibliographies
    "david_project_disk")
   (LaTeX-add-xcolor-definecolors
    "black2"))
 :latex)

