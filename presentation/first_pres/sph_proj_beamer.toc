\beamer@subsectionintoc {0}{1}{Population of mass transfer accretion disks}{2}{0}{0}
\beamer@sectionintoc {1}{Introduction}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Intro}{5}{0}{1}
\beamer@subsectionintoc {1}{2}{Problem}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Possible solutions}{8}{0}{1}
\beamer@sectionintoc {2}{Accretion disks}{10}{0}{2}
\beamer@subsectionintoc {2}{1}{Accretion disks}{11}{0}{2}
\beamer@subsectionintoc {2}{2}{Properties of disks}{12}{0}{2}
\beamer@subsectionintoc {2}{3}{Disks and evolution}{14}{0}{2}
\beamer@subsectionintoc {2}{4}{Accretion Disks in mass transferring binary systems}{14}{0}{2}
\beamer@sectionintoc {3}{Disk mass loss}{16}{0}{3}
\beamer@subsectionintoc {3}{1}{General idea}{17}{0}{3}
\beamer@subsectionintoc {3}{2}{Types of mass loss from disk}{18}{0}{3}
\beamer@sectionintoc {4}{Methods}{23}{0}{4}
\beamer@subsectionintoc {4}{1}{Codes}{24}{0}{4}
\beamer@sectionintoc {5}{Results}{26}{0}{5}
\beamer@subsectionintoc {5}{1}{Population of mass transfer accretion disks}{27}{0}{5}
\beamer@subsectionintoc {5}{2}{Disk mass loss}{27}{0}{5}
\beamer@sectionintoc {6}{Summary}{27}{0}{6}
